let button1;
let button2;
var reset = false;
var time = 0;

//The start page
function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255,0,0);
  button1 = createButton('No');
  button1.position(width/2 + 50, height/2);
  button1.mousePressed(gotoblack);

  button2 = createButton('Yes');
  button2.position(width/2 - 50, height/2);
  button2.mousePressed(makenewbutton);

}
//What happens after you have pressed the no button
function draw() {
  if (reset == true) {
    console.log(frameCount);
    if(frameCount > time+300){
      console.log("done");
      clear();
      setup();
      redraw();
      reset= false;
    }
  }
}

//the functions of the no button
function gotoblack() {
  reset = true;
  background(0);
  button1.hide();
  button2.hide();
  time = frameCount;

}
//the functions of the yes button
function makenewbutton() {
  button1.hide();
  button2 = createButton('Yes');
  button2.position(random(width),random(height));
  button2.mousePressed(makenewbutton);

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
