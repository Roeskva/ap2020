function startIdentifying() {
  interval = setInterval(classifyGender, 500);
}

function classifyGender() {
  captureFlipped = ml5.flipImage(capture); //Flipping the images input from webcam
  classifier.classify(captureFlipped, handleGender);//Classifing the captureFlipped according to our model and call handleGender and passing the result or error as an argument
  captureFlipped.remove();//Remove the stored image to allow for another image to be assigned
}

function handleGender(error, results) {
  if (error) {
    console.error(error);//if anything is assign to the error argument then display it in the console
    return;
  }


  label = results[0].label //The "result" argument parsed from classifier.classify() is an array with the most proable label at index 0
  arrayOfResults.push(label);//We add this label to an array
  console.log(label);//prints the label to the console
  counter++;
  if (counter > 10) {
    clearInterval(interval);//Stops the interval from looping
    userGender = determineGender();//assigning the reurned value from determineGender();
  }
}

//This function counts all the gender labels and returns the label most present as the result. 
function determineGender() {
  imageBeingTaken = false;
  capture.show();//Show the webcam feed again

  let numberOfMale = 0;
  let numberOfFemale = 0;
  for (let i = 0; i < arrayOfResults.length; i++) {
    if (arrayOfResults[i].localeCompare("Female") == 0) {
      numberOfFemale++;
    }
    if (arrayOfResults[i].localeCompare("Male") == 0) {
      numberOfMale++;
    }
  }
  console.log(numberOfMale);
  console.log(numberOfFemale);

  if (numberOfMale > numberOfFemale) {
    console.log('Male');
    return "Male";
  }
  if (numberOfMale < numberOfFemale) {
    console.log('Female');
    layout.genderChange.show();
    return "Female";
  }
}
