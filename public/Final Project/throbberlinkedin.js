var x = 500;
var speed = 1;

function throbber() {
  x = x + speed;

  if ((x > 600 - 20) || (x < 500)) {
    //If the object reaches either edge, multiply speed by -1 to turn it around.
    speed = speed * -1;
  }

  fill(150);
  text('Identifying Your Gender...', 435, 410);

  //Grey line
  noStroke();
  fill(200);
  rect(500, 370, 100, 5);

  //Blue, moving line
  noStroke();
  fill(9, 122, 176);
  rect(x, 370, 20, 5);
}
