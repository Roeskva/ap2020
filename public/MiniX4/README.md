**YES/NO**

![ScreenShot](minix4billede.png)

https://roeskva.gitlab.io/ap2020/MiniX4

In this project i made two buttons. One YES button and one NO button.
The Yes button replicates it self on the screen as many times as you press it. 
The No button turns the screen black then waits a few seconds and starts over. 

It is an simplification of the cookie system used today on the internet. If you dont agree (press no) with the side, there wont be anything.
The page wont let you do anything but just turn black. 
If you agree (press yes) with the page there is always more that you can agrre with and say yes to.
There is no limit to how much you can share or agree to on the internet. And there is no turning back to disagree once you already said yes.


In this project I made use of diffenrent buttons, functions and variables. I learned to use frameCount to make a time loop. 
With the theme "capture all" the first thing i thought of was cookies. So i wanted to do something that could illustrate the problematics of agreing and disagreing with no knowlegde of what happens.
I didnt want to make any big consequenses with the theme, but make it simple so it was easy to understand and see what happens. 

