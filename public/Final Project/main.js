let layout;
let genderDecider;
let capture;
let captureFlipped;
let label = "";
let container;
let counter = 0;
let interval;

let arrayOfResults = [];

let imageBeingTaken = false;
let userGender;

function preload() {
  layout = new Layout();
  layout.preloads();

  classifier = ml5.imageClassifier("./tm-my-image-model/model.json");//Loading out model
  jobs = loadJSON("jobsandsalary.json")//Loading our json

}

function setup() {
  layout.setups();
  layout.genderChange.hide();

  //Setting up videostream from webcam
  capture = createCapture(VIDEO);
  capture.size(200, 200);

  //We are using css to add the video html element on top of the canvas element.
  capture.style('border-radius', '50%');
  capture.style('overflow', 'hidden');
  capture.style('position', 'absolute');
  capture.style('left', '135px');
  capture.style('top', '220px');
}

function draw() {
  layout.draws();

  //Runs when the program is classifing the gender
  if (imageBeingTaken == true) {
    capture.hide();//hides webcam feed
    layout.takePicture.hide();//Hides the takePicture button
    fill(250, 250, 250, 240);
    rect(0, 0, windowWidth, windowHeight);//Create rectangle that covers canvas
    throbber();//Runs the throbber
  }
  //If the userGender is assigned a value then this will run
  if (typeof userGender !== "undefined") {
    displayJobs(userGender);
    displaySalary(userGender);
  }

}

function pictureTaken() {
  imageBeingTaken = true;
  startIdentifying();//Start the classifcation of the images
}

function genderChangePressed() {
  //opens new window in browser
  window.open("https://www.rigshospitalet.dk/afdelinger-og-klinikker/julianemarie/gynaekologisk-klinik/undersoegelse-og-behandling/sygdomme-og-behandling/Sider/transseksualitet-og-koensidentitet.aspx");
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);//Whenever the window is resizes the Canvas follows
}


function displayJobs(gender) {
  fill(0);
  textSize(18);
  if (gender.localeCompare("Female") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
    for (let t = 0; t < jobs[0].jobsforwomen.length; t++) {//Goin through element in array
      let posY;
      posY = 50 * t + 400;//Increments the y position of the text
      text(jobs[0].jobsforwomen[t], 1100, posY);//Display the job
    }
  }

  if (gender.localeCompare("Male") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
    for (let t = 0; t < jobs[0].jobsformen.length; t++) {//Going through each element in array
      let posY;
      posY = 50 * t + 400;//Increments the y position of the text
      text(jobs[0].jobsformen[t], 1100, posY);//Display the job
    }
  }
}

function displaySalary(gender) {
  fill(0);
  textSize(38);
  if (gender.localeCompare("Female") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
    text(jobs[1].salarywomen[0] + " DKK", 1090, 250);//display salary
  }
  if (gender.localeCompare("Male") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
    text(jobs[1].salarymen[0] + " DKK", 1090, 250);//display salary
  }
}

//Used to locate posisitions when creating the Layout
/*
function mousePressed() {
  console.log("x:" + mouseX);
  console.log("y:" + mouseY);
}
*/
