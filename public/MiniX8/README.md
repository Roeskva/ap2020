![Screenshot](minixbillede8.png)

https://roeskva.gitlab.io/ap2020/MiniX8

The CookBook

In this project Mads Marschall and I worked together. We decided that we wanted to make a recipe collection as we thought the ingredients would be fun to search through. 

We have made our code in a theme which is cookbook. Our variables have names related to the theme to make it more fun to read and also more relatable to the actual program and what it does. 

Our first thought was that you should be able to search ingredients that you have in your fridge and a recipe would show up. But we came to the conclusion that it would be too many variables considered how many different ingredients there could be. So we decided to make button that you could choose from and then one or more recipes would show up.

We did manage to make a search engine that can search any word in the recipes, but we did not include a big number of recipes in our JSON file. 
At first we had found a json file that had over thirty thousand recipes, but our little search engine could not handle them and crashed the program. We then decided to write our own json file with some of our own recipes. 

In this progress we got a good understanding of how json files work and how restricted they are. We had to be careful with how we wrote our recipes as we could fx not make line shift in the howto section to make it easier for us self to read. This we related to how computers don’t read and understand aesthetics as we do. People has tendencies to go for the visually pretty and aesthetically nice to read, and sometimes dismiss what is uncomfortable to look at despite it being readable. Machines and the coding programs only need a text to be readable in their construction and syntax and does not dismiss on aesthetics. 

Some coding becomes a fight between the aesthetical pretty and the coding possible. When we are able to call our variables and functions something related to recipes and a cookbook it is because the different interfaces are able to translate between each program. So our efforts to have a theme in our writing is solely for us and the reader of the code – it has no effect on how the machines read our code. 

We tried to replicate some of the writing as in Vocable Code. Like some of our variables are named so it could be said aloud: “let theseBeYourOptions” fx. 
We have not done it in the same extent as Vocable Code, sp when we use theseBeYourOptions it does not read in the same way. Our code is long in itself because of all the intricate names for the functions and variables. 
