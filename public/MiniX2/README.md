![Screenshot](minix2billede.png)

https://roeskva.gitlab.io/ap2020/MiniX2

In this MiniX I ended up with three emojis instead of just two. 

The first emoji I made was the white wine glass. It does not have any big wider cultural and political aspect, other than a friend of mine said she was missing it.
I made use of the arc function to make the upper of the glass. It made it easy to replica how the wine and glas as to seperate things.
I tried during this exercise to make my coding as simple as possible, because I last time mainly copied code and editited it without actually understanding the code.

The second emoji I made was the girl with bangs. I made it because a friend said she needed an emoji to represent herself and others with bangs. 
I thought it referenced the issue with the different skin colours, the diversity in display of men and women. I dont think it has as deep ties to the past, if any, as skin tones does.
But the diversity issue can be strecthed far out over it boundaries and used in almost any situation. I think it can be dangerous to just say, that you need something to represent someone or somehting,
without stopping up and considering the actual need. I also think such things need to bring up the question of how much influence an emoji is allowed to have.

The last emoji i made was a Kinder Maxi chocolate. I had a bit of trouble making this one as i wanted it not to be alligned with the coordinate system. 
So it did not turn out as good and simple as I wanted, and I could not figure out how to make the text crooked other than splitting up the letters.
I made it for fun first, but then I thought about how emojis could be commercialised for undercover ads. 
The phones might not just be phones, the food we might associate with chains and the electronics can have connotation in the form of a company behind it. 

The emojis are seen and demanded to be neutral but also diverse. And I think that there is a lot more than just diversity at stake.

The whole outcome of my canvas with emojis has become kinda ironic as it ended up symbolizing the cis white girl of the modern western world. 
The pink, the bangs, the white and the brand chocolate. 



