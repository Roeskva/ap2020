function setup() {
  createCanvas(640,480);
  print("hello world");
}
function draw() {
background(255,204, 229)
//bobble
noStroke();
fill(255,255,204, 175)
ellipse(332,215,8,11)
//wine and glass
stroke(255,255,255)
fill(255,255,204, 125)
  arc(300, 200, 110, 145, 0, PI, OPEN);
fill(255,255,255)
strokeWeight(4)
//stilk
  line(300,275,300,340)
//bottom
line(270,340,330,340)

//The white in KinderMaxien
noStroke();
beginShape()
vertex(365,85)
vertex(395,55)
vertex(540,150)
vertex(515,180)
endShape(CLOSE)

//The red in KinderMaxien
fill(255,0,0)
beginShape()
vertex(365,84)
vertex(382,68)
vertex(529,163)
vertex(515,179)
endShape(CLOSE)

//Janne - a friend: head
noStroke();
fill(255,229,204)
ellipse(80,90,50,50)
//Hair
noFill();
	stroke(90,51,17);
  strokeWeight(5);
  	bezier(79,65,55,63,50,109,61,108);
  	bezier(79,65,52,61,49,111,57,107);
    bezier(79,65,115,62,105,107,99,108);
	  bezier(79,65,115,62,106,109,103,106);
    line(79,65,79,77)
 line(74,65,73,77)
 line(87,65,88,77)
 line(92,67,93,77)
 line(83,65,83,77)
 line(70,66,69,77)
 line(94,68,94,77)
//eyes
 stroke(0,0,0);
point(71,88)
point(90,88)

//mouth
stroke(240,5,5)
strokeWeight(3)
arc(80, 97, 19, 19, 0, PI, OPEN);

//text
noStroke();
fill(109,205,246)
textSize(15)
text('K',419,89)
text('i',431,93)
text('n',437,97)
text('d',447,106)
text('e',458,110)
text('r',468,116)
text('M',480,129)
text('a',495,134)
text('x',505,141)
text('i',515,148)


//to find coordinates easily
if (mouseIsPressed) {
console.log (mouseX,mouseY);
}


}
