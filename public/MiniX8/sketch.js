//The variables for the cookbook
let cookbook;
let goingThroughTheCookBook;
let thisBeYourRecipe;
let recipeName = "No ingredients selected";

let theseBeYourOptions = [];

//the ingredients
let groceries = ['Egg', 'Milk', 'Lemon', 'Chocolate', 'Lemongrass', 'Water', 'Cinchona Bark', 'Limes', 'Citric Acid', 'Junipers', 'Pepper', 'Mace', 'Sugar Cane', 'Beer', 'Salt', 'Flour', 'Sugar', 'Oil', 'Butter']
let choiceOfgroceries = [];
let next;
let previous;


function preload() {
  cookbook = loadJSON('./data/VeryImportantDataRecipe.json');//Loads our JSON to the cookbook variable
}

//The main text is in setup
function setup() {
  goingThroughTheCookBook = new FridgeSniffer();//Create a new instance of FridgeSniffer
  createCanvas(windowWidth, windowHeight);
  groceriesAvailable();//Calls the groceriesAvailable - see delacaring section for more info

  //The back and forth buttons in the right buttom corner.
  textFont('Georgia');
  next = createButton('>');
  next.position(width - 100, height - 100);
  //styling
  next.style('background-color', '#F1948A');
  next.style('border-radius', '50%');
  next.style('border-color', 'white');
  next.style('color', 'white');

  previous = createButton('<');
  previous.position(width - 200, height - 100);
  //styling
  previous.style('background-color', '#F1948A');
  previous.style('border-radius', '50%');
  previous.style('border-color', 'white');
  previous.style('color', 'white');
/*
  Callback functions:
  A callback function is a function that takes another function as an argument.
  what this means is that you have the higher level function  - in this case it would be mousePressed(),
  which descides when the callback function is called

*/
  next.mousePressed(maybeAnotherRecipe);//when it is pressed it will call maybeAnotherRecipe() as a callback
  previous.mousePressed(maybeAnotherRecipe);//when it is pressed it will call maybeAnotherRecipe() callback
}

function draw() {
  background(211, 84, 0);

  //Position of the recipes when found
  textFont('Georgia');
  textSize(35);
  fill(123, 36, 28);
  text('Welcome to our Cookbook', windowWidth / 2, windowHeight / 6);
  textSize(30);
  text('Press a button to look through it', windowWidth / 2, windowHeight / 4);
  textSize(25);
  text('Recipe: ' + recipeName, windowWidth / 2, windowHeight / 3);

  mmmhhh();

}

//this lets you view other recipes with the ingredient
function maybeAnotherRecipe() {
  let id = this.elt.textContent;
  let idLowercase = id.toLowerCase();
  if (id == ">" && theseBeYourOptions.length) {
    theseBeYourOptions.push(theseBeYourOptions[0])
    theseBeYourOptions.shift();
  }
  if (id == "<" && theseBeYourOptions.length) {
    theseBeYourOptions.unshift(theseBeYourOptions[theseBeYourOptions.length - 1]);
    theseBeYourOptions.pop();
  }
}

//this shows the recipe on the side
function mmmhhh() {
  if(theseBeYourOptions.length) {
    let howToMakeIt =  theseBeYourOptions[0].howto;
    textSize(20);
    text(howToMakeIt, width/2, height/3+50, 300, 350);
    recipeName = theseBeYourOptions[0].name;
    let pos = 0;
    let amountOfIngredients = theseBeYourOptions[0].ingredients.length;
    let ingredients = theseBeYourOptions[0].ingredients;
    for(let p = 0; p < amountOfIngredients; p++) {
      pos = pos + p+25
      text(ingredients[p], width-300, height/3+50+pos);

    }
  }
}
//position of the search buttons and the two lines instead of one long (sorry it is so ugly)
function groceriesAvailable() {
  let ink;
  let ink2;
  let ink3 = 0;
  for (let i = 0; i < groceries.length; i++) {
    ink = height / 5 + 50 * i;

    if (ink > height - 50) {
      ink2 = ink - 50 * i;
      ink2 = ink2 + (i - ink3) * 50 - 50;
      choiceOfgroceries.push(createButton(groceries[i]));
      choiceOfgroceries[i].position(width / 4, ink2);
      //styling
      choiceOfgroceries[i].style('background-color', '#F1948A');
      choiceOfgroceries[i].style('border-radius', '25%');
      choiceOfgroceries[i].style('border-color', 'white');
      choiceOfgroceries[i].style('color', 'white');

    } else {
      ink3 = i;
      choiceOfgroceries.push(createButton(groceries[i]));
      choiceOfgroceries[i].position(width / 8, ink);
      //styling
      choiceOfgroceries[i].style('background-color', '#F1948A');
      choiceOfgroceries[i].style('border-radius', '25%');
      choiceOfgroceries[i].style('border-color', 'white');
      choiceOfgroceries[i].style('color', 'white');
    }

    choiceOfgroceries[i].hasThisBeenClicked = false;
    choiceOfgroceries[i].mousePressed(chooseThisFood);

  }
}

//the function of pressing a button and the machine searching for the buttons name (and the button changing color)
function chooseThisFood() {
  if (this.hasThisBeenClicked) {
    this.style('background-color', '#F1948A');
    console.log("option1");
    this.hasThisBeenClicked = false;


  } else {
    console.log("option2");
    this.style('background-color', '#641E16');
    for(let i = 0; i < choiceOfgroceries.length; i++){
      if(choiceOfgroceries[i].elt.textContent != this.elt.textContent){
        this.hasThisBeenClicked = false;
        choiceOfgroceries[i].style('background-color', '#F1948A')
      }
    }
    this.hasThisBeenClicked = true;

  }
  let id = this.elt.textContent;
  let idLowercase = id.toLowerCase();
  //console.log(idLowercase);

  theseBeYourOptions = searchTheCookbook(idLowercase);
}

function searchTheCookbook(tag) {
  return goingThroughTheCookBook.searchForIngredients(tag);
  console.log("print this and then it should work");
}

//The search engine that looks through the cookbook
class FridgeSniffer {
  constructor() {}

  searchForIngredients(tag) {
    let possibleRecipes = [];
    let word = tag;
    word.toLowerCase();
    for (let i = 0; i < Object.keys(cookbook).length; i++) {
      for (let j = 0; j < cookbook[i].ingredients.length; j++) {

        if (-1 !== cookbook[i].ingredients[j].indexOf(word)) {

          possibleRecipes.push(cookbook[i]);

        }
      }
    }
    return possibleRecipes;
  }
  setthisBeYourRecipe(recipe) {
    thisBeYourRecipe = recipe;
  }
  getHowTo() {
    return thisBeYourRecipe.howto;
  }
  removeDuplicates(array) {
    return array.filter(function(a, b) {
      return array.indexOf(a) === b
    });
  }
  getListOfIngredients() {

  }
}
//So it doesnt show the same recipe multiple times
function removeDuplicates(array) {
  return array.filter(function(a, b) {
    return array.indexOf(a) === b
  });
}
