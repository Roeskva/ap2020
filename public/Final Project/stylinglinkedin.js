class Layout {
  constructor() {
    this.canvas;

    //Defining the fonts
    this.leagueSpartan;
    this.fontLight;
    this.fontRegular;

    //Defining the buttons
    this.takePicture;
    this.genderChange;
  }
  preloads() {
    //loading of fonts
    this.leagueSpartan = loadFont('LeagueSpartan.otf');
    this.fontLight = loadFont('OpenSans-Light.ttf');
    this.fontRegular = loadFont('OpenSans-Regular.ttf');
  }
  setups() {
    this.canvas = createCanvas(windowWidth, windowHeight);
    this.canvas.style('position', 'relative');
    background(240, 241, 242);

    //Button "Suggest Jobs"
    this.takePicture = createButton('Suggest Jobs');
    this.takePicture.position(680, 370);
    this.takePicture.mousePressed(pictureTaken);
    this.takePicture.style('background-color', '#097ab0');
    this.takePicture.style('border-radius', '4px');
    this.takePicture.style('border-color', '#097ab0');
    this.takePicture.style('color', 'white');
    this.takePicture.style('font-size', '16px');
    this.takePicture.style('padding', '7px 55px');

    //Button "Upgrade Salary"
    this.genderChange = createButton('Upgrade Salary');
    this.genderChange.position(680, 600);
    this.genderChange.mousePressed(genderChangePressed);
    this.genderChange.style('background-color', '#FAFAFC');
    this.genderChange.style('border-radius', '4px');
    this.genderChange.style('border-color', '#F1D01B');
    this.genderChange.style('color', '#F1D01B');
    this.genderChange.style('font-size', '16px');
    this.genderChange.style('padding', '7px 55px');
  }
  draws() {
    //Blue bar at top
    fill(30, 60, 74);
    noStroke();
    rect(0, 0, windowWidth, 58);

    //Linkedin logo
    fill(9, 122, 176);
    rect(70, 9, 40, 40, 3);
    fill(255);
    textFont(this.leagueSpartan);
    textSize(32);
    text('in', 75, 43);

    //Search bar
    fill(210);
    rect(125, 9, 300, 40, 2);
    fill(75);
    textSize(15);
    text('Search', 160, 35);

    //Search loop
    noFill();
    stroke(75);
    strokeWeight(2);
    ellipse(142, 27, 12, 12);
    line(147, 33, 152, 38);

    //Big white square
    fill(250);
    strokeWeight(1);
    stroke(150);
    rect(90, 110, 900, 560);

    //Little smaller blue square
    fill(49, 155, 191);
    rect(90, 110, 900, 240);

    //dots and lines
    noStroke();
    fill(255, 150);
    ellipse(826, 295, 10, 10);
    ellipse(953, 271, 10, 10);
    ellipse(928, 232, 10, 10);
    ellipse(840, 246, 10, 10);
    ellipse(887, 209, 10, 10);
    ellipse(979, 145, 10, 10);
    ellipse(697, 156, 10, 10);
    ellipse(612, 317, 10, 10);
    ellipse(539, 255, 10, 10);
    ellipse(286, 127, 10, 10);
    ellipse(339, 237, 10, 10);
    ellipse(472, 324, 10, 10);
    ellipse(245, 172, 10, 10);
    ellipse(134, 189, 10, 10);

    stroke(255, 120);
    line(826, 295, 953, 271);
    line(953, 271, 928, 232);
    line(928, 232, 840, 246);
    line(840, 246, 826, 295);
    line(887, 209, 928, 232);
    line(979, 145, 887, 209);
    line(979, 145, 928, 232);
    line(928, 232, 990, 214);
    line(953, 271, 990, 271);
    line(979, 145, 990, 152);
    line(976, 322, 990, 299);
    line(976, 322, 953, 271);
    line(976, 322, 931, 352);
    line(976, 322, 990, 339);
    line(890, 352, 826, 295);
    line(962, 352, 826, 295);
    line(750, 352, 826, 295);
    line(840, 246, 887, 209);
    line(887, 209, 698, 157);
    line(698, 157, 840, 246);
    line(613, 319, 826, 295);
    line(613, 319, 840, 246);
    line(613, 319, 698, 157);
    line(613, 319, 539, 255);
    line(697, 156, 539, 255);
    line(286, 127, 539, 255);
    line(286, 127, 697, 156);
    line(339, 237, 539, 255);
    line(339, 237, 286, 127);
    line(472, 324, 612, 317);
    line(472, 324, 539, 255);
    line(472, 324, 339, 237);
    line(245, 172, 339, 237);
    line(245, 172, 286, 127);
    line(134, 189, 245, 172);
    line(134, 189, 286, 127);
    line(697, 156, 557, 110);
    line(286, 127, 348, 110);
    line(697, 156, 812, 110);
    line(887, 209, 878, 110);

    //Profile picture
    stroke(230);
    strokeWeight(4);
    fill(250);
    ellipse(235, 320, 200, 200);

    //Profile text
    fill(0);
    noStroke();
    textSize(25);
    textFont(this.fontRegular);
    text('Your name', 120, 455);

    textSize(20);
    text('Gender: ' + userGender, 120, 490);

    textSize(17);
    textFont(this.fontLight);
    text('Region, Country', 120, 520);

    textSize(15);
    textFont(this.fontLight);
    text('Occupation and education', 120, 550);

    textSize(17);
    fill(49, 155, 191);
    text('Contact information', 120, 580);




    //Icon | Work
    noStroke();
    fill(210);
    rect(1200, 9, 5, 5);
    rect(1200 + 9, 9, 5, 5);
    rect(1200 + 18, 9, 5, 5);
    rect(1200, 9 + 9, 5, 5);
    rect(1200 + 9, 9 + 9, 5, 5);
    rect(1200 + 18, 9 + 9, 5, 5);
    rect(1200, 9 + 18, 5, 5);
    rect(1200 + 9, 9 + 18, 5, 5);
    rect(1200 + 18, 9 + 18, 5, 5);
    textFont(this.fontRegular);
    textSize(12);
    fill(210);
    text('Work', 1185, 48);
    triangle(1220, 40, 1220 + 16, 40, 1220 + 8, 48);
    rect(1170, 0, 0.5, 58);

    //Icon | You
    text('You', 1112, 48);
    triangle(1140, 40, 1140 + 16, 40, 1140 + 8, 48);
    ellipse(1135, 21, 24, 24);

    //Icon | Notifications
    text('Notifications', 1018, 48);
    push();
    stroke(210);
    strokeWeight(2.2);
    noFill();
    beginShape();
    curveVertex(1042, 32);
    curveVertex(1042, 32);
    curveVertex(1042 + 4, 32 - 5);
    curveVertex(1054 - 6, 11 + 4);
    //midten
    curveVertex(1054, 11);
    //højre punkt
    curveVertex(1054 + 6, 11 + 4);
    curveVertex(1066 - 4, 32 - 5);
    curveVertex(1066, 32);
    curveVertex(1066, 32);
    endShape();
    line(1042, 32, 1066, 32);
    line(1042 + 4, 32 - 5, 1066 - 4, 32 - 5);
    pop();
    fill(210);
    ellipse(1054, 34, 5, 5);

    //Icon | Messages
    text('Messages', 935, 48);
    push();
    stroke(210);
    strokeWeight(2.2);
    noFill();
    push();
    translate(8, 0);
    //first square
    line(945, 11, 945 + 16, 11);
    line(945, 11, 945, 11 + 13);
    line(945, 24, 948, 24);
    line(961, 11, 961, 14);
    //second square
    line(952, 18, 952, 18 + 13);
    line(952, 18, 952 + 16, 18);
    line(952 + 16, 18, 952 + 16, 18 + 16);
    line(952, 18 + 13, 952 + 11, 18 + 13);
    line(952 + 11, 18 + 13, 952 + 16, 18 + 16);
    //lines
    line(957, 23, 957 + 6, 23);
    line(958, 26, 958 + 4, 26);
    pop();
    pop();

    //Icon | Job
    text('Job', 895, 48);
    push();
    stroke(210);
    strokeWeight(2.2);
    noFill();
    rect(900, 9, 9, 5, 1);
    rect(891, 14, 27, 18);
    rect(891, 20, 27, 0.5);
    pop();

    //Icon | Network
    text('Network', 820, 48);
    push();
    stroke(210);
    strokeWeight(2.2);
    noFill();
    line(833, 32, 833 + 24, 32);
    line(833, 32, 833, 32 - 8);
    line(833 + 16, 32, 833 + 16, 32 - 8);
    line(833 + 24, 32, 833 + 24, 32 - 8);
    line(833, 32 - 9, 833 + 4, 32 - 12);
    line(833 + 16, 32 - 9, 833 + 12, 32 - 12);
    line(833 + 24, 32 - 8, 855, 22);
    ellipse(841, 15, 10, 10);
    ellipse(852, 18, 6, 6);
    pop();

    //Icon | Home
    text('Home', 760, 48);
    stroke(210);
    strokeWeight(2.2);
    noFill();
    push();
    translate(-2, 0);
    //house
    line(768, 32, 768 + 24, 32);
    line(768, 32, 768, 20);
    line(768 + 24, 32, 768 + 24, 20);
    //roof
    line(766, 20, 766 + 14, 11);
    line(766 + 14, 11, 766 + 28, 20);
    pop();

    //Box to the right
    stroke(220);
    fill(250);
    rect(1050, 110, 350, 560);

    //Text in box to the right
    fill(0);
    noStroke();
    textFont(this.fontRegular);
    textSize(20);
    text('Your estimated annual salary:', 1090, 160);
    text('Job recommendations:', 1090, 330);

    //Finding the coordinates. Used when styling the layout
    //if (mouseIsPressed) {
    //console.log(mouseX, mouseY);
    //}

  }
}
