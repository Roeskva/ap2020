> MiniX 1 

![ScreenShot](minix1billede.png)

My first experince with coding was fun.
The constant trying and deleting made it easy going and required me to constantly reflect on the codes.
Each new line of code had a mystery to it. What it did, if it did anything.
It was a good challenge to figure out, what different changes did to the outcome.

The process was very different from a normal writing process as in every change could delete everything.
Or make it disappear at least. The consequenses of each line is bigger in coding than normal writing.
I also made a lot more copy and paste. I did not write as much as i changed already written code.

The programming makes me curious and ready to get projects with demands to what i should make.
It also makes me excited to make more than just shapes and colours.
Real working websites, programs and games.

https://roeskva.gitlab.io/ap2020/MiniX1
