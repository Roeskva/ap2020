let rad = 75;
let xpos, ypos;

let xspeed = 2.8;
let yspeed = 2.2;

let xdirection = 1;
let ydirection = 1;

function setup() {
createCanvas(windowWidth, windowHeight)
noStroke();
  frameRate(30);
  ellipseMode(RADIUS);
  xpos = width / 2;
  ypos = height / 2;
}

function draw() {
background(255);

xpos = xpos + xspeed * xdirection;
ypos = ypos + yspeed * ydirection;

if (xpos > width - rad || xpos < rad) {
    xdirection *= -1;
  }
  if (ypos > height - rad || ypos < rad) {
    ydirection *= -1;
  }

fill(0);
ellipse(xpos,ypos,rad,rad);

fill(255);
textSize(25);
text('Please wait',250,240);
text('Wait some more',300,340);
text('One moment please',500,70);
text('Please wait',700,600);
text('Give me five',100,300);
text('Dont be impatient',20,500);
text('Wait',500,270);
text('Just wait',1000,500);
text('Please wait',800,240);
text('Wait some more',300,340);
text('Wait some more',1200,700);
text('Please wait',1050,333);
text('Wait some more',567,788);
text('One moment please',345,90);
text('Please wait',200,600);
text('Give me five',600,300);
text('Dont be impatient',500,20);
text('Wait',1200,89);
text('Just wait',1000,150);
text('Please wait',500,500);
text('Wait some more',700,740);
text('Wait',129,108);
text('Dont be impatient',871,368);
text('Give me five',1187,217);
text('Be patient',628,187);
text('Just a second',181,390);
text('Hold on',976,568);
text('Wait',1244,419);
text('Please wait',819,133);
text('Just wait',119,716);
text('Hold on',828,460);
text('One moment please',897,530);


if (mouseIsPressed) {
console.log (mouseX,mouseY);
}


}
