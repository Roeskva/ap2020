let virus = [];
let start = 0;
let end = 0;
let infected = 0;

function setup() {
  createCanvas(800,400);

  //The yellow bubles which is my virus
  for (let i = 0; i < 35; i++) {
    let x = random(width);
    let y = random(height);
    let r = 20;
    virus[i] = new Virus(x,y,r);

  }
}

//My virus
class Virus {
  constructor(x,y,r) {
    this.x = x;
    this.y = y;
    this.r = r;
  }

  move() {
    this.x = this.x + random(-4, 4);
    this.y = this.y + random(-4, 4);
  }

  show() {
    stroke(241,249,18, 65);
    strokeWeight(3);
    noFill();
    ellipse(this.x,this.y,this.r*2);
  }
}

function draw() {
  background(0);

  //When you start
  if(mouseX-10 > 75 && mouseX-10 < 175 && mouseY-10 > 0 && mouseY-10 < 20) {
    start = 1;
  }
  if (start == 1 && mouseX-10 > 75 && mouseX-10 < 175 && mouseY-10 > 0 && mouseY-10 < 20) {
    textSize(60);
    fill(241,249,18);
    text('Dont touch the Virus!',100,225);
  }

  //When you win
  if(mouseX-10 > 600 && mouseX-10 < 700 && mouseY-10 > 380 && mouseY-10 < 400) {
    end = 1;
  }
  if (end == 1 && mouseX-10 > 600 && mouseX-10 < 700 && mouseY-10 > 380 && mouseY-10 < 400) {
    textSize(60);
    fill(241,249,18);
    text('You are safe!',200,225);
  }

  //My virus
  for (let i = 0; i < virus.length; i++) {
    virus[i].move();
    virus[i].show();

  //If touching the virus you get infected
 let d = dist(mouseX-10,mouseY-10, virus[i].x,virus[i].y);
 if (d < virus[i].r){
   fill(241,249,18);
   textSize(60);
   text('You are infected!',150,225);

}
}
    //The blue dot aka you the player
    fill(30,30,200);
    stroke(255);
    strokeWeight(1);
    //I made the dot a little after the mouse, so you could see it better.
    ellipse(mouseX-10,mouseY-10,10,10);


  //The start and end points
  fill(200,20,20);
  noStroke();
  rect(75,0,100,20);
  rect(600,380,100,20);
  fill(255);
  textSize(20);
  text('Start', 100, 17)
  text('Finnish', 620,397);


  if (mouseIsPressed) {
    console.log (mouseX,mouseY);
  }
}




//Inspiration:
//https://editor.p5js.org/kchung/sketches/Bya-96Xz4
