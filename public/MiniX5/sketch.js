let rad = 75;
let xpos, ypos;

let xspeed = 2.8;
let yspeed = 2.2;

let xdirection = 1;
let ydirection = 1;

let words = ['One moment please','Please wait','Wait some more','Give me five','Dont be impatient','Wait','Just wait','Hold on','Be patient',];

function setup() {
  createCanvas(windowWidth, windowHeight)
  noStroke();
  frameRate(30);
  ellipseMode(RADIUS);
  xpos = width / 2;
  ypos = height / 2;
}

function draw() {

  background(255);

  xpos = xpos + xspeed * xdirection;
  ypos = ypos + yspeed * ydirection;

  if (xpos > width - rad || xpos < rad) {
    xdirection *= -1;
  }
  if (ypos > height - rad || ypos < rad) {
    ydirection *= -1;
  }

//the black circle
  fill(0);
  ellipse(xpos,ypos,rad,rad);

// the text
  fill(255);
  textSize(25);
  text(words[0], random(width), random(height));
  text(words[1], random(width), random(height));
  text(words[2], random(width), random(height));
  text(words[3], random(width), random(height));
  text(words[4], random(width), random(height));
  text(words[5], random(width), random(height));
  text(words[6], random(width), random(height));
  text(words[7], random(width), random(height));
  text(words[8], random(width), random(height));


  if (mouseIsPressed) {
    console.log (mouseX,mouseY);
  }

}
