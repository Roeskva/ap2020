![Screenshot](minix3billede.png)

https://roeskva.gitlab.io/ap2020/MiniX3

I dont know why, but I really cant make the link work. I checked the path and everything looks exactly like my past links, so I dont understand. 
If you want to see the program you can copy the code into atom and run it. Sorry for the inconveince, I am working to sovle the problem...

This program is an ellipse that bounces on the canvas as the DVD logo did on television screens. 
I made the background white and the text white. Then i made a balck ellipse. I used variables let to set the speed, positions and directions of the ball.
I also lowered the framerate for the motion. Then i made an if condition so that is the ball hit the width and height of the screen it would bounce.
The text is drawn after the ellipse and therefor appear on the ellipse when it goes over (under) it.
That way i made the effect of a flashlight (just opposite colours of the normal).

I actually think that the design of the program is boring, but that is the thing about throbbers. They are only there to project time and that can be boring.
The text is mostly neutral but some of the lines are more humanlike and that makes it a little bit arrogant which i think is funny. 
I like the aspect with the text and the background both being white and therefor making the text invisible when the ball is not moving under it. 
It is such a simple trick but it makes all the difference. 

I think the throbber is more direct in its message as it is actually telling one to wait, and not just subtly passing time. 
This can make the user more frustated as the situation is still not explained but made very explicit. 

Inspiration and help: 

https://p5js.org/examples/motion-bounce.html : Used for the motion

DVD Guy used for inspiration of the motion

