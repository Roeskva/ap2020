
![Screenshot](minix6billede.png)

https://roeskva.gitlab.io/ap2020/MiniX6

My game is a virus game, where if you touch the virus you get infected.
There is a starting point that tells you not to touch the virus when you have your dot over it. And a finnish point that tells you, you are safe. 
The intention is to get from the starting point to finnish without touching any of the yellow circles which is the infection. 
If you touch the yellow circles you get a message which says you are infected. 
You are represented by a blue dot, that moves just behind the mouse on the canvas. 

The blue dot is made to move with the mouseX and mouseY but -10, so you can better see the dot. 
The virus is the ellipses which is made in a class, where there is a constructor, a move and a show. I then call for the ellipses in a for loop, because i made the virus a variable.
If the blue dot is too close to the center of one of the ellipsis the message of 'You got infected' will come up. 
If the blue dot touches the areal of the start and finnish rectangles it will also show a message.

The characteristics of my obejcts and the programming is based on my view of how fx viruses look. I see them as something round and yellow. That is how is see bacteria and virus. 
My abstraction comes from the images you see elsewhere of bacteria and virus - and of a childish thought that it should be yellow because snot is yellow.

This whole thought behind how a virus looks is something i have defined. This shows how programmers define a lot of things, which they maybe dont have any idea about how actually looks. 
These wider implications of abstration and object-oriented programming can make deep cultural cliffs because not everybody sees the same as i do. 
I therefor believe it to be important both as a programmer to reflect upon choices but also as a user to not rely too much on what you see unless it is rooted in actual knowlegde.


Inspiration and help:

https://editor.p5js.org/kchung/sketches/Bya-96Xz4

https://www.youtube.com/watch?v=TaN5At5RWH8